import os
for file in os.listdir('.'):
    if file.endswith("Agree.txt"):
         with open(file+'replaced_without_neutral','w+')  as write_f:
              with open(file,'r') as  read_f:
                  for line in read_f:
                      splited_line = line.split('@')
                                            
                      if splited_line[1].strip().replace('\n','') == 'positive':
                          splited_line[1] = 'Bullish'
                          new_line = splited_line[0]+'\t\t'+splited_line[1].replace('\n','')+'\n'
                          write_f.write(new_line)

                      elif splited_line[1].strip().replace('\n','') == 'negative':
                          splited_line[1] = 'Bearish'
                          new_line = splited_line[0]+'\t\t'+splited_line[1].replace('\n','')+'\n'
                          write_f.write(new_line)

#                      elif splited_line[1].strip().replace('\n','') == 'neutral':
#                          splited_line[1] = 'Neutral'
#                      new_line = splited_line[0]+'\t\t'+splited_line[1].replace('\n','')+'\n'
#                      write_f.write(new_line)

