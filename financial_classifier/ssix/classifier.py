# -*- coding: utf-8 -*-

import logging
import re
import sys,os

from functools import reduce
sys.path.append(os.path.dirname(os.path.realpath(__file__))+"/../")
from ssix.models import create_stopwords_set, create_word_embedding_model
from ssix.analyzers import create_predictor_analyzer, create_aspect_analyzer, \
    create_rule_analyzer, create_analyzed_sentence


class SSIXClassifier(object):
    __logger = logging.getLogger(__name__)

    def __init__(self, language, config):
        self.__logger.info("Initializing SSIX classifier for %s", language)
        self.__logger.debug("Config: %s", config)
        self.__stopwords = create_stopwords_set(language)
        self.__word_embedding_model = create_word_embedding_model(language, config)
        self.__predictor = create_predictor_analyzer(language, self.__word_embedding_model)
        self.__aspect_extractor = create_aspect_analyzer(language, config, self.__word_embedding_model)
        self.__rule_classifier = create_rule_analyzer(language)
        self.language = language

    def classify(self, text):
        self.__logger.info("Classifying '%s'", text)
        text = re.sub(r'(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))', '', text, flags=re.MULTILINE)
        analysed = create_analyzed_sentence(self.language, text)
        aspects = self.__extract_aspects(analysed)
        classification = self.__do_classification(analysed)

        # TODO: figure out a better aggregation
        sentiment = 0.0 if not classification \
            else reduce(lambda x, y: x + y, classification.values()) / len(classification)

        targets = {analysed.entities[x]: {"label": x, "sentiment": classification[x]} for x in analysed.entities}
        self.__logger.info("\ntext='{}'\nsentiment={}\ntargets={}\naspects={}".format(text, sentiment, targets, aspects))
        return sentiment, targets, aspects

    def __extract_aspects(self, analysed):
        if self.__aspect_extractor is not None:
            has_polarized_subordination = analysed.has_polarized_subordination()
            aspects = {}
            if has_polarized_subordination:
                if analysed.main_clause is not None:
                    main_clause = analysed.main_clause.lower()
                    aspects[main_clause] = self.__aspect_extractor.analyse(main_clause)

                if analysed.subordinate_clause is not None:
                    subordinate_clause = analysed.subordinate_clause.lower()
                    aspects[subordinate_clause] = self.__aspect_extractor.analyse(subordinate_clause)

            else:
                aspects[analysed.sentence] = self.__aspect_extractor.analyse(analysed.sentence)

            return aspects

    def __do_classification(self, analysed_sentence):
        classification = {}
        sentence = analysed_sentence.sentence
        has_polarized_subordination = analysed_sentence.has_polarized_subordination()

        if len(analysed_sentence.entities) <= 1:
            for mention, _ in analysed_sentence.entities.items():
                if has_polarized_subordination:
                    if analysed_sentence.main_clause is not None:
                        main_clause = analysed_sentence.main_clause.lower()
                        classification[mention] = self.__classify(main_clause)
                else:
                    classification[mention] = self.__classify(sentence)

        else:
            if has_polarized_subordination:
                main_clause = analysed_sentence.main_clause
                subordinate_clause = analysed_sentence.subordinate_clause

                for mention, _ in analysed_sentence.entities.items():
                    if main_clause and mention in main_clause.lower():
                        classification[mention] = self.__classify(main_clause.lower())
                    elif subordinate_clause and mention in subordinate_clause.lower():
                        classification[mention] = self.__classify(subordinate_clause.lower())
                    elif main_clause:
                        classification[mention] = self.__classify(main_clause.lower())
            else:
                for mention, _ in analysed_sentence.entities.items():
                    classification[mention] = self.__classify(sentence)

        # Even without NEs we still try to predict something.
        if not classification:
            classification[analysed_sentence.sentence] = self.__classify(analysed_sentence.sentence)

        return classification

    def __classify(self, sentence):
        if self.__is_covered_by_rule_classifier(sentence):
            return self.__classify_by_rule(sentence)
        else:
            return self.__classify_by_predictor(sentence)

    def __classify_by_rule(self, sentence):
        self.__logger.info("Calculating sentiment for '%s'", sentence)
        return self.__rule_classifier.polarity_scores(sentence)['compound']

    def __classify_by_predictor(self, sentence):
        self.__logger.info("Predicting sentiment for '%s'", sentence)
        return self.__predictor.predict(sentence)

    def __is_covered_by_rule_classifier(self, sentence):
        sentence = sentence.replace('!', ' !').replace('.', '')
        for token in sentence.lower().split():
            if token not in self.__stopwords:
                if token in self.__rule_classifier.lexicon:
                    return True
        return False


