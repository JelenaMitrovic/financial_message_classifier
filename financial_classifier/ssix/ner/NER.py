# -*- coding: utf-8 -*-

from os import path


class NER(object):
    def __init__(self):
        lexfile = path.join(path.dirname(__file__), 'lexicon_generator/company_lexicon.txt')
        with open(lexfile) as f:
            self.__companies = {line.split(';')[0].strip(): line.split(';')[1].strip() for line in f}

    def resolve(self, sentence):
        ngrams = set([])
        tokens = sentence.lower().replace('.', '').replace(',', '').replace('!', '').split(' ')
        entities = {}

        for i in range(0, len(tokens) - 2):
            ngram = tokens[i] + ' ' + tokens[i + 1] + ' ' + tokens[i + 2]
            ngrams.add(ngram)

        for i in range(0, len(tokens) - 1):
            ngram = tokens[i] + ' ' + tokens[i + 1]
            ngrams.add(ngram)

        for i in range(0, len(tokens)):
            ngram = tokens[i]
            ngrams.add(ngram)

        for ngram in ngrams:
            if ngram in self.__companies:
                if ngram is not 'a':
                    entities[ngram] = self.__companies[ngram]

        return entities
