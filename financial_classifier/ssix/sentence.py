# -*- coding: utf-8 -*-

import logging

from ssix.ner import NER


class BaseSentence(object):
    _logger = logging.getLogger(__name__)
    __ner = NER()

    def __init__(self, sentence):
        if not sentence:
            raise ValueError("Invalid sentence.")

        self.main_clause = None
        self.subordinate_clause = None
        self.sentence = sentence
        self.entities = self.__ner.resolve(sentence)
        if not self.entities:
            self._logger.warning("No entities detected.")
        else:
            self._logger.info("NEs: %s", self.entities)

    def has_coordination(self):
        for token in self.sentence.lower().split():
            if token in self._get_coordinations():
                return True
        return False

    def has_polarized_subordination(self):
        for token in self.sentence.lower().split():
            if token in self._get_polarity_shifting_subordinations():
                return True
        return False

    def _get_coordinations(self):
        raise NotImplementedError

    def _get_polarity_shifting_subordinations(self):
        raise NotImplementedError

    def __str__(self):
        return "s=[{}], main=[{}], sub=[{}], NEs={}"\
            .format(self.sentence, self.main_clause, self.subordinate_clause, self.entities)


class EnglishSentence(BaseSentence):

    __polarity_shifting_subordinating_conjunction_set = \
        {'but', 'after', 'although', 'as', 'as if', 'as long as', 'as much as', 'as soon as', 'as though', 'because',
         'before', 'even', 'even if', 'even though', 'if', 'if only', 'if when', 'if then', 'in as much', 'despite',
         'in order that', 'just as', 'least', 'now', 'now since', 'now that', 'now when', 'once', 'provided',
         'provided that', 'rather that', 'since', 'so that', 'supposing', 'than', 'that', 'though', 'till', 'unless',
         'until', 'when', 'whenever', 'where', 'whereas', 'where if', 'wherever', 'whether', 'which', 'while', 'who',
         'whoever', 'why', 'however'}

    __coordinating_conjunction_set = {'for', 'and', 'nor', 'but', 'or', 'yet', 'so'}

    def __init__(self, sentence):
        super(EnglishSentence, self).__init__(sentence)
        self.__process_subordination()

    def _get_coordinations(self):
        return self.__coordinating_conjunction_set

    def _get_polarity_shifting_subordinations(self):
        return self.__polarity_shifting_subordinating_conjunction_set

    def __process_subordination(self):
        if not self.has_polarized_subordination():
            return

        sentence_tokens = self.sentence.lower().split(' ')
        sub_conj = ''
        for subToken in self.__polarity_shifting_subordinating_conjunction_set:
            if subToken in sentence_tokens:
                sub_conj = subToken
                break

        # shape II
        # conj SUBORD , MAIN
        if (self.sentence.lower().index(sub_conj) == 0) and (self.sentence.index(',') != -1):
            self.main_clause = self.sentence.split(',')[1]
            self.subordinate_clause = self.sentence.split(',')[0]

        else:
            # shape I
            # MAIN conj SUBORD
            self.main_clause = self.sentence.split(sub_conj)[0].strip()
            if len(self.sentence.split(sub_conj)) > 1:
                self.subordinate_clause = self.sentence.split(sub_conj)[1].strip()
            else:
                self.subordinate_clause = ''
