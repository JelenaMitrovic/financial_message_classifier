import re
from nltk.corpus import wordnet as wn
from nltk.stem import WordNetLemmatizer
import sys
import os
import nltk
import numpy as np
import requests
import json
from sklearn import preprocessing
from sklearn.preprocessing import LabelBinarizer
current_project_path = os.path.dirname(os.path.realpath(__file__)) + "/../"
print(current_project_path)
# improved list from Stone, Denis, Kwantes (2010)
STOPLIST= """
a about above across afterwards again against all almost alone along already also although always am among amongst amoungst amount an and another any anyhow anyone anything anyway anywhere are around as at be
became because become becomes becoming been before beforehand behind being below beside besides between beyond bill both bottom but by call can
cannot cant co computer con could couldnt cry de describe
detail did didn do does doesn doing don done down due during
each eg eight either eleven else elsewhere empty enough etc even ever every everyone everything everywhere except few fifteen
fify fill find fire first five for former formerly forty found four from front full further get give go
had has hasnt have he hence her here hereafter hereby herein hereupon hers herself him himself his how however hundred i ie
if in inc indeed interest into is it its itself keep last latter latterly least less ltd
just
kg km
made make many may me meanwhile might mill mine more moreover most mostly move much must my myself name namely
neither never nevertheless next nine no nobody none noone nor not nothing now nowhere of off
often on once one only onto or other others otherwise our ours ourselves out over own part per
perhaps please put rather re
quite
rather really regarding
same say see seem seemed seeming seems serious several she should show side since sincere six sixty so some somehow someone something sometime sometimes somewhere still such system take ten
than that the their them themselves then thence there thereafter thereby therefore therein thereupon these they thick thin third this those though three through throughout thru thus to together too top toward towards twelve twenty two un under
until up unless upon us used using
various very very via
was we well were what whatever when whence whenever where whereafter whereas whereby wherein whereupon wherever whether which while whither who whoever whole whom whose why will with within without would yet you
your yours yourself yourselves
"""
#STOPLIST = frozenset(w for w in STOPLIST.split() if w)

def get_stoplist():
    return [w for w in STOPLIST.split()]

def is_noun(tag):
    return tag in ['NN', 'NNS', 'NNP', 'NNPS']


def is_verb(tag):
    return tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']


def is_adverb(tag):
    return tag in ['RB', 'RBR', 'RBS']


def is_adjective(tag):
    return tag in ['JJ', 'JJR', 'JJS']


def penn_to_wn(tag):
    if is_adjective(tag):
        return wn.ADJ
    elif is_noun(tag):
        return wn.NOUN
    elif is_adverb(tag):
        return wn.ADV
    elif is_verb(tag):
        return wn.VERB
    else:
        return None


def get_lemmatized_token(token, tag):
    wn_tag = penn_to_wn(tag)
    wordnet_lemmatizer = WordNetLemmatizer()
    if wn_tag == None:
        return token
    else:
        return wordnet_lemmatizer.lemmatize(token, pos=wn_tag)


def isAlphanumeric(token):
    pattern = "[A-Za-z]"
    match = re.findall(pattern, token)
    return True if len(match) > 0 else False


def load_finance_dictionary(files_path):
    dictionary = []
    for file_path in files_path:
        with open(file_path,'r') as lines:
            dictionary += [word.replace("\n","") for word in get_finance_terms(file_path)]
    return dictionary

def get_investopedia_terms(sentence):
    path= [current_project_path + "docs/finance_terms.txt"]
    investopedia_terms = load_finance_dictionary(path)
    new_sentence = sentence[:].lower()
    for term in investopedia_terms:
        if len(term.split()) >= 2:
            new_sentence = re.sub(term,term.replace('\s','_'),new_sentence)
    return new_sentence

def tokenize(text, dictionary=None):
    #path_investopedia = "../data/"
    text = get_investopedia_terms(text)
    text = re.sub("(\$[A-Za-z]+)","",text)
    text_tokenize = nltk.word_tokenize(normalize_finance_terms(text))
    if dictionary is None: 
        tokenized_sentence = [token for token in text_tokenize]
    else:
        tokenized_sentence = [token for token in text_tokenize if token in dictionary]
    return [token for token in tokenized_sentence if token not in get_stoplist() and isAlphanumeric(token)]
#    return [token for token in tokenized_sentence]


PATH = "/home/sousa/test_macedo/"
sys.path.append(PATH + 'store_twits_sentiment_analysis/')

emotion_to_number = {
    'Bullish': 0,
    'Bearish': 1
}
number_to_emotion = {
    0: 'Bullish',
    1: 'Bearish'
}

emotion_to_number_neutral = {
    'Bullish': 1,
    'Bearish': 0,
    'Neutral': 2
}
number_to_emotion_neutral = {
    1: 'Bullish',
    0: 'Bearish',
    2: 'Neutral'
}


def convert_number_to_emotion(number):
    return number_to_emotion[int(number)]


def convert_emotion_to_number(emotion):
    return emotion_to_number[emotion]


def convert_emotions_to_numbers(emotions):
    emotions_id = {}
    for emotion in emotions:
        emotions_id.append(emotion_to_number[emotion])
    return emotions_id


def convert_number_to_onehot(numbers):
    number_onehot = {}
    lb = preprocessing.LabelBinarizer()
    lb.fit(numbers)
    for number in numbers:
        number_onehot[number] = lb.transform([number])[0]
    return lb, number_onehot


def convert_number_to_emotion_neutral(number):
    return number_to_emotion_neutral[int(number)]


def convert_emotion_to_number_neutral(emotion):
    return emotion_to_number_neutral[emotion]


def convert_emotions_to_numbers_neutral(emotions):
    emotions_id = []
    for emotion in emotions:
        emotions_id.append(emotion_to_number_neutral[emotion])
    return np.array(emotions_id)


def separate_training_testing_set(input, output, training_per=0.8):
    sample_count = len(input)
    training_cont = int(sample_count * training_per)
    training_input_samples = input[:training_cont]
    training_output_samples = output[:training_cont]
    testing_input_samples = input[training_cont + 1:]
    testing_output_samples = output[training_cont + 1:]

    return training_input_samples, training_output_samples, testing_input_samples, testing_output_samples


def normalize_sentence(sentence):
    sentence = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', '', sentence)
    sentence = re.sub('\$[A-Z\n]+', '', sentence)
    sentence = re.sub('\@[A-Za-z0-9]+', '', sentence)
    regex = re.compile('[^a-zA-Z\s,-.]')
    sentence = regex.sub('', sentence)
    # sentence = re.sub('[0-9]+','',sentence)
    sentence = re.sub("[,.-]", ' ', sentence)
    return sentence.strip()


def get_vocabulary(sentences):
    vocabulary = dict()
    cont = 0
    for sentence in sentences:
        for token in sentence:
            if not (token.lower() in vocabulary):
                vocabulary[token.lower()] = cont
                cont += 1
    print("amount of words: ", len(vocabulary))
    return vocabulary


def get_one_hot_word_vectors(sentences):
    vocabulary = dict()
    cont = 0
    word_id_vocabulary = get_vocabulary(sentences)
    lb = LabelBinarizer()
    lb_model = lb.fit(word_id_vocabulary)

    for sentence in sentences:
        for token in sentence:
            if token.lower() in word_id_vocabulary:
                vocabulary[token.lower()] = lb_model.transform(word_id_vocabulary[token.lower()])[0]
            else:
                raise Exception('My error!')
    return vocabulary


def get_sentence_vocabulary_id(sentence, vocabulary, max_sentence_size):
    in_tuple = (max_sentence_size, 1)
    w2v_tokenized_sentence = np.empty(in_tuple, dtype='int')
    w2v_tokenized_sentence.fill(-1)

    for i, token in enumerate(sentence):
        if token.lower() in vocabulary:
            vec = vocabulary[token.lower()]
        else:
            vec = -1.0
    return w2v_tokenized_sentence


def get_sentences_vocabulary_id(tokenized_sentences, vocabulary, max_sentence_size):
    in_tuple = (len(tokenized_sentences), max_sentence_size, 1)
    in_vectors = np.empty(in_tuple, dtype='int')
    in_vectors.fill(-1)

    for i, tokenized_sentence in enumerate(tokenized_sentences):
        in_vectors[i] = get_sentence_vocabulary_id(tokenized_sentence, vocabulary, max_sentence_size)
    # print(in_vectors[i])
    return in_vectors

def get_finance_sentiment_terms(file_path):
    word_list = []
    with open(file_path, 'r') as file:
        for line in file:
            if line.strip().lower() == 'uncertainty':
                break
            line = re.sub('\([0-9]+\)', '', line.lower().strip())
            line = re.sub('[^a-zA-Z]+', '', line)
            word_list.append(line)
    return word_list

def normalize_finance_terms(sentence):
    sentence = re.sub('\([0-9]+\)', '', sentence.lower().strip())
    sentence = re.sub('[^a-zA-Z\s_]+', '', sentence)
    return sentence

def get_finance_terms(file_path):
    word_list = []
    with open(file_path, 'r') as file:
        for line in file:
            line = re.sub('\([0-9]+\)', '', line.lower().strip())
            line = re.sub('[^a-zA-Z\s_]+', '', line)
            word_list.append(line)
    return word_list

def get_finance_sentiment_terms_by_sentiment(file_path):
    word_list = []
    sentiment_words = dict()
    words = []

    with open(file_path, 'r') as file:
        for line in file:

            if line.strip().lower() == 'negative':
                current_flag = 0
            elif line.strip().lower() == 'positive':
                current_flag = 1
            elif line.strip().lower() == 'uncertainty':
                current_flag = 2
            line = re.sub('\([0-9]+\)', '', line.lower().strip())
            line = re.sub('[^a-zA-Z]+', '', line.lower().strip())

            # if current_flag == 2:
            #               print(line)
            if not (line in words):
                #               print(current_flag)
                words.append(line)
                sentiment_words[line] = current_flag

    return sentiment_words


def get_finance_sentences_sentiment_vector(sentences, sentence_size, file_path):
    dict = get_finance_sentiment_terms_by_sentiment(file_path)
    lb = LabelBinarizer()
    lb.fit(dict.values())
    in_tuple = (len(sentences), sentence_size, len(lb.transform([2])[0]))
    sentiment_sentence = np.empty(in_tuple, dtype='int')
    sentiment_sentence.fill(-1)

    for i, sentence in enumerate(sentences):
        for j, token in enumerate(sentence):
            if j < sentence_size:
                if token in dict:
                    sentiment_sentence[i][j] = lb.transform([dict[token]])
                    # print(lb.transform([dict[token]]))
                else:
                    sentiment_sentence[i][j] = lb.transform([2])
    return sentiment_sentence


def get_count_word_sentiment_vector(sentences, sentence_size, file_path):
    negative = positive = neutral = 0
    in_tuple = (len(sentences), sentence_size, 3)
    count_sentiment_sentence = np.empty(in_tuple, dtype='int')
    count_sentiment_sentence.fill(-1)
    for i, sentence in enumerate(sentences):
        num_words = len(sentence)
        print("count words= ", str(num_words))
        for j, token in enumerate(sentence):
            if j < sentence_size:
                if token in dict:
                    if dict[token] == 0:
                        negative += 1
                    elif dict[token] == 1:
                        positive += 1
                    else:
                        neutral += 1
                else:
                    neutral += 1
        for j, token in enumerate(sentence):
            if j < sentence_size:
                count_sentiment_sentence[i][j][0] = negative / (
                num_words if num_words < sentence_size else sentence_size)
                count_sentiment_sentence[i][j][1] = positive / (
                num_words if num_words < sentence_size else sentence_size)
                count_sentiment_sentence[i][j][2] = neutral / (
                num_words if num_words < sentence_size else sentence_size)
        print(count_sentiment_sentence)
    return count_sentiment_sentence


def get_token_finance_valid_words(sentence, wordList):
    new_sentence = []
    sentence = normalize_sentence(sentence)
    #    sentence = nltk_u.get_lemmatized_tokens(sentence)
    for word in nltk.word_tokenize(sentence):
        # print(word)
        if word.lower().strip() in wordList:
            new_sentence.append(word.lower().strip())
    return new_sentence


def get_joined_core_sentence_graphene(sentence):
    url = 'http://localhost:8080/simplification/text'
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    r = requests.post(url, '{"text": \"' + str(sentence) + '\" }', headers=headers)
    coreSentences = []
    #    print(sentence)
    #    print(r)
    for simplifiedsentence in r.json()['simplifiedSentences']:
        for coreSentence in simplifiedsentence['coreSentences']:
            coreSentences.append(coreSentence['text'])
            for contextSentence in coreSentence['contextSentences']:
                coreSentences.append(contextSentence['text'])

    return ' '.join(word for word in coreSentences)


def get_core_sentence_graphene(sentence):
    url = 'http://localhost:8080/simplification/text'
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    r = requests.post(url, '{"text":\"' + str(sentence) + '\" }', headers=headers)
    #    print(r.json())
    coreSentences = []

    for simplifiedsentence in r.json()['simplifiedSentences']:
        for coreSentence in simplifiedsentence['coreSentences']:
            coreSentences.append(coreSentence['text'])
            for contextSentence in coreSentence['contextSentences']:
                coreSentences.append(contextSentence['text'])

    return coreSentences
