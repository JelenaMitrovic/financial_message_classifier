# -*- coding: utf-8 -*-

import argparse
import logging
import json
import sys
import re
import nltk
import numpy as np
import csv

from os.path import join, abspath, dirname, expanduser
from nltk import RegexpParser
from nltk.tag.stanford import StanfordPOSTagger
from sklearn.metrics.pairwise import cosine_similarity

from ssix.aspect.myutils.CMUTweetTagger import CMUTweetTagger
from ssix.aspect.myutils.tokenisers import Nokeniser, TwokeniseTokeniser
from ssix.config import load_from_file
from ssix.analyzers import English
from ssix.classifier import create_aspect_analyzer
from ssix.models import create_word_embedding_model

from collections import defaultdict

from ssix.ner import NER

this_dir = dirname(abspath(__file__))


class AspectExpressionExtractor(object):
    __logger = logging.getLogger(__name__)

    GRAMMAR_CMU = """
                        NP: {<D>?<A>*<N|S|Z|\^>+}       # need to escape reserved character "\^"
                            {<\#>}                      # need to escape reserved character "\#"; extract hashtags, too
                        PP: {<P><NP>}                  # Chunk prepositions followed by NP
                        VP: {<V.*><R|T.*|P>?<NP|PP|CLAUSE>+}    # Chunk verbs and their arguments
                        CLAUSE: {<NP><VP>}             # Chunk NP, VP
                     """  # PP ff. code from http://www.nltk.org/book/ch07.html ch 4.1
    GRAMMAR_TWITIE = """
                        NP: {<DT|PRP\$>?<JJ.*>*<NN.*>+} # need to escape reserved character "$"
                            {<HT>}                      # extract hashtags, too
                        PP: {<IN><NP>}                  # Chunk prepositions followed by NP
                        VP: {<VB.*><RB.*|IN>?<NP|PP|CLAUSE>+}    # Chunk verbs and their arguments
                        CLAUSE: {<NP><VP>}             # Chunk NP, VP
                     """  # PP ff. code from http://www.nltk.org/book/ch07.html ch 4.1

    LEXICALISATIONS = join(this_dir, "data", "lexicalisations.json")
    COORDINATING_CONJUNCTION = join(this_dir, "data", "coordinating_conjunction_list.txt")
    SUBORDINATING_CONJUNCTION = join(this_dir, "data", "subordinating_conjunction_list.txt")

    def __init__(self, config, word_embedding_model, lexicalisations_file=LEXICALISATIONS):
        self.analyser = config.ssix.aspects.analyser
        tools_root_path = abspath(expanduser(config.ssix.aspects.external_tools.root_path))

        self.CMU_TAGGER_CMD = config.ssix.aspects.external_tools.cmu_tagger.cmd
        self.CMU_JAR = join(tools_root_path, config.ssix.aspects.external_tools.cmu_tagger.path)

        stanford_tagger_cfg = config.ssix.aspects.external_tools.stanford_tagger
        self.STANFORD_JAR = join(tools_root_path, stanford_tagger_cfg.path)
        self.STANFORD_MODEL_FILE = join(tools_root_path, stanford_tagger_cfg.twitter_model)
        self.STANFORD_MODEL_TAGGER = join(tools_root_path, stanford_tagger_cfg.english_model)

        self.__logger.info("Initialising analyser '%s'...", self.analyser)
        self.__lexicalisations = self.__load_lexicalisations(join(this_dir, lexicalisations_file))
        self.ner = NER()
        self.stop = []  # TODO: rather use a set here - more efficient (also change in _load_exclusion_words())

        if self.analyser == "cmu":
            self.tokeniser = Nokeniser()  # CMU does its own tokenisation so we use the "Nokeniser" :)
            self.tagger = CMUTweetTagger(run_tagger_cmd=self.CMU_TAGGER_CMD, tagger_jar=self.CMU_JAR)
            self.chunker = RegexpParser(self.GRAMMAR_CMU)

        elif self.analyser == "twitie":
            self.tokeniser = TwokeniseTokeniser()
            # Using Stanford tagger with TwitIE-tagger model
            self.tagger = StanfordPOSTagger(self.STANFORD_MODEL_FILE, path_to_jar=self.STANFORD_JAR, encoding='utf8')
            self.chunker = RegexpParser(self.GRAMMAR_TWITIE)

        else:
            raise LookupError("Analyser {} isn't implemented. Available analysers: twittie or cmu".format(self.analyser))

        self.__word_embedding_model = word_embedding_model

    def analyse(self, text):
        self.__logger.info("Extracting aspects of '%s'..", text)
        tagged = self.__preprocess(text)
        candidates = self.__extract_patterns(tagged)
        filtered_candidates = self.__filter_candidates(candidates, text)
        return self.__select_top_aspects_list(filtered_candidates, self.__lexicalisations)

    def __preprocess(self, text):
        return self.__tag(text)

    def __tag(self, text):
        if isinstance(text, list):
            return self.tagger.tag_sents([self.tokeniser.tokenize(item) for item in text])
        return self.tagger.tag(self.tokeniser.tokenize(text))

    def __extract_patterns(self, tagged_text):
        if tagged_text is None or not tagged_text:
            return []
        elif isinstance(tagged_text[0], list):
            result = []
            for message in tagged_text:
                patterns = self.__extract_subtrees(self.chunker.parse(message), tags=["NP", "VP"])
                result.append(patterns)

            return result

        else:
            return self.__extract_subtrees(self.chunker.parse(tagged_text), tags=["NP", "VP"])

    def __extract_subtrees(self, chunked_tree, tags):
        """
            Extract subtrees which match 'tags'
        """
        subs = []

        for node in chunked_tree:
            if isinstance(node, nltk.tree.Tree) and node.label() in tags:
                subs.append(node)

            for child in node:  # we're only going two levels deep in our grammar so no full recursion needed
                if isinstance(child, nltk.tree.Tree) and child.label() in tags:
                    subs.append(child)

        return subs

    def __filter_candidates(self, cands, text):
        self.__load_exclusion_words(text)

        if isinstance(cands, list) and len(cands) > 0:
            if not isinstance(cands[0], nltk.tree.Tree):
                for sent_list in cands:
                    for cand in sent_list:
                        for node in cand:
                            try:
                                for node_string, node_label in node:
                                    if node_string.lower() in self.stop:
                                        # FIXME: remove entire candidate only or just the closest subtree thingo?
                                        sent_list.remove(cand)

                            except ValueError:
                                continue

            else:
                for cand in cands:
                    try:
                        for node_string, node_label in cand:
                            if isinstance(node_string, tuple):
                                if node_string[0].lower() in self.stop:
                                    # FIXME: remove entire candidate only or just the closest subtree thingo?
                                    cands.remove(cand)
                            else:
                                if node_string.lower() in self.stop:
                                    # FIXME: remove entire candidate only or just the closest subtree thingo?
                                    cands.remove(cand)

                    except ValueError:
                        continue

        return cands

    def __load_exclusion_words(self, message):

        # TODO: review filtering - should we remove all of this?
        # TODO: if changing 'stop' to set, change here also
        if isinstance(message, list):
            for msg in message:
                for opinion_target_term in self.__get_opinion_targets(msg):
                    self.stop.extend(opinion_target_term)
                self.stop.extend(self.ner.resolve(msg).keys())  # all entities are lowercase
                self.stop.extend([i for i in message if i.startswith("$")])
                self.stop.extend([i for i in message if i.startswith("http")])
        else:
            for opinion_target_term in self.__get_opinion_targets(message):
                self.stop.extend(opinion_target_term)
            self.stop.extend(self.ner.resolve(message).keys())  # all entities are lowercase
            self.stop.extend([i for i in message if i.startswith("$")])
            self.stop.extend([i for i in message if i.startswith("http")])

    def __select_top_aspects_list(self, candidates, lexicalisations):

        if not candidates:
            return {}

        if not isinstance(candidates[0],
                          nltk.tree.Tree):  # candidates is a list of lists of Tree (rather than a list of Tree)
            ranked_aspects = self.__rank_candidates_list(candidates, lexicalisations)
            top_aspects = []
            for sentence in ranked_aspects:
                if sentence != {}:
                    top_aspects.append(self.__select_top_aspect(sentence))

                else:  # sometimes there are no aspects / candidates
                    top_aspects.append((None, None, None))

            return top_aspects

        else:  # candidates is a list of Tree
            ranked_aspects = self.__rank_candidates(candidates, lexicalisations)
            top_aspect, top_text, top_score = self.__select_top_aspect(ranked_aspects)
            aspect_text = " ".join([string for string, postag in top_text.leaves()]).strip()
            return {"aspect_name": top_aspect, "aspect_text": aspect_text, "score": float(top_score)}

    def __get_opinion_targets(self, message):
        rule = '(JJ(R|S)?)\s(NN(S|PS|P)?)+|(RB(R|S)?)\s(JJ(R|S)?)+\s(NN(S|PS|P)?)+|(JJ(R|S)?)\s(NN(S|PS|P)?\sIN)'
        word_opinion_target = []
        st = StanfordPOSTagger(self.STANFORD_MODEL_TAGGER, self.STANFORD_JAR)
        clauses = self.__get_main_clauses(message)
        for clause in clauses:
            sent = ''
            postags = st.tag(nltk.word_tokenize(clause.strip()))
            for postag in postags:
                sent = sent + " " + postag[1]

            if not (re.search(rule, sent) is None):

                m = re.search(rule, sent)
                string_lenght = 1
                is_valid_position = False
                for postag in postags:
                    if string_lenght - 1 == m.span()[1]:
                        is_valid_position = False
                    elif string_lenght == m.span()[0] or is_valid_position:
                        is_valid_position = True
                        word_opinion_target.append(postag[0])
                    string_lenght += len(postag[1]) + 1

        return word_opinion_target

    def __get_main_clauses(self, sentence):
        main_clause_list = []
        main_clause = ''
        st = StanfordPOSTagger(self.STANFORD_MODEL_TAGGER, self.STANFORD_JAR)
        postags_sentence = st.tag(nltk.word_tokenize(sentence))
        with open(self.COORDINATING_CONJUNCTION) as f:

            coord_conj = f.read().splitlines()
        with open(self.SUBORDINATING_CONJUNCTION) as f2:
            subord_conj = f2.read().splitlines()

        for i, token in enumerate(nltk.word_tokenize(sentence)):
            if token.lower() in coord_conj and postags_sentence[i][1] == 'CC':
                main_clause_list.append(main_clause)
                main_clause = ''
            elif token.lower() in subord_conj and postags_sentence[i][1] == 'CC':
                break
            else:
                main_clause = main_clause + ' ' + token
        main_clause_list.append(main_clause)
        return main_clause_list

    def __select_top_aspect(self, candidates):
        top_cand_per_aspect = []
        for aspect, cands in candidates.items():
            top_cand, top_score = sorted(cands, key=lambda x: x[1], reverse=True)[0]
            top_cand_per_aspect.append((aspect, top_cand, top_score))

        return sorted(top_cand_per_aspect, key=lambda x: x[2], reverse=True)[0]

    def __rank_candidates_list(self, candidates, lexicalisations):
        """
            Rank "Aspect Expression Candidates" against the possible lexicalisations

            :param candidates: a list of lists of nltk.tree.Tree containing chunks that are candidates for aspect expressions

            :returns: a dictionary - key=aspect name, value = list of tuples (text candidate, score)
        """
        self.__logger.info("Ranking the aspect expression candidates...")

        # candidates is a list of lists of Tree (rather than a list of Tree)
        if not isinstance(candidates[0], nltk.tree.Tree):
            ranked_all = []
            for sentence_candidates in candidates:
                ranked_all.append(self.__rank_candidates(sentence_candidates, lexicalisations))

            return ranked_all

        else:  # candidates is a list of Tree
            return self.__rank_candidates(candidates, lexicalisations)

    def __rank_candidates(self, candidates, lexicalisations):

        scored = defaultdict(list)
        for candidate in candidates:
            evaluated = self.__score_candidate_against_lex(candidate, lexicalisations)
            for aspect, score in evaluated:
                scored[aspect].append((candidate, score[0][0]))

        return scored

    def __score_candidate_against_lex(self, candidate, lexicalisations):
        """
            Compare chunk 'candidate' against the lexicalisations available

            :param candidate - a nltk.tree.Tree representing an aspect expression candidate
            :param lexicalisations - a dictionary of available lexicalisations

            :return list of (aspect, score) - for each aspect in the model, a score of how similar it is to this candidate
        """
        aspect_scores = []
        words_to_compare = []
        # for NPs, we use adjectives and nouns
        if candidate.label() == "NP":
            for child, child_pos in candidate.leaves():  # FIXME: possibly go 1 level deeper?? check what this covers
                if child_pos.startswith("N") or child_pos.startswith("JJ"):
                    words_to_compare.append(child)

        # for VPs, we use verbs and nouns
        elif candidate.label() == "VP":
            for child, child_pos in candidate.leaves():  # FIXME: possibly go 1 level deeper?? check what this covers
                if child_pos.startswith("V") or child_pos.startswith("N"):
                    words_to_compare.append(child)

        # compare each aspect (its lexicalisations) and the words we scheduled for comparison
        for aspect_name, lexis in lexicalisations.items():
            score = 0
            n = 0
            for lex in lexis:  # TODO: could do phrase comparison with Indra and/or a more sophisticated scoring
                for word in words_to_compare:
                    for lex_word in lex.split(" "):
                        score += self.__score_words(word, lex_word)
                        n += 1

            try:
                if isinstance(score, float):
                    aspect_scores.append((aspect_name, [[score / n]]))
                else:
                    aspect_scores.append((aspect_name, score / n))
            except ZeroDivisionError:
                aspect_scores.append((aspect_name, [[0]]))

        return aspect_scores

    def __score_words(self, w1, w2):

        v1 = self.__get_word_embedding_vector(w1)
        v2 = self.__get_word_embedding_vector(w2)

        if v1 is None or v2 is None:  # unknown words yield a 'None'
            return 0.0

        return cosine_similarity(np.array(v1).reshape(1, -1), np.array(v2).reshape(1, -1))

    def __get_word_embedding_vector(self, w):
        """
            Look up word in w2v

            :param w - the word to be looked up

            :return corresponding vector from word2vec (vector of -1's if word not in model)
        """
        if w in self.__word_embedding_model:
            return self.__word_embedding_model[w]
        else:
            return None

    def __load_lexicalisations(self, lexicalisations_file):
        self.__logger.info("Loading lexicalisations...")
        with open(lexicalisations_file) as lexi:
            jdata = json.load(lexi)
            d = {}
            for item in jdata:
                d[item["id"]] = item["lexicalisations"]

            return d


def __read_gs_json(f):
    with open(f) as infile:
        jdata = json.load(infile)
        txtmessages = list({jrecord['message'] if 'message' in jrecord else jrecord['title'] for jrecord in jdata})
        txtmessages.sort()
        return txtmessages


def __write_results(aspects_results, outfile, threshold=0.8):
    """
        Write messages & aspect extraction results to csv file
    """

    print("Writing {} entries in {}".format(len(aspects_results), outfile))

    header = ['index', 'message', 'aspect_name', 'aspect_text', 'score']
    with open(outfile, 'w', encoding='utf-8') as out:
        writer = csv.DictWriter(out, fieldnames=header)
        writer.writeheader()

        for msg_idx, aspectres in enumerate(aspects_results):
            message, aspects = aspectres
            data = dict(index=msg_idx, message=message)

            if not aspects:
                data.update(dict(aspect_name='N/A', aspect_text='N/A', score='N/A'))
            elif aspects['score'] >= threshold:
                data.update(aspects)

            writer.writerow(data)


def __gsfilecmd(args, extractor):
    messages = __read_gs_json(args.gsfile)
    messages = messages[:args.limit] if args.limit > 0 else messages

    print("Analyzing {} messages".format(len(messages)))

    allaspects = []
    for msg in messages:
        aspects = extractor.analyse(msg)
        if args.out is not None:
            allaspects.append((msg, aspects))
        else:
            print(aspects)

    if allaspects:
        __write_results(allaspects, args.out, args.threshold)


def __txtcmd(args, extractor):
    print(extractor.analyse(args.message))


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(levelname)s %(name)s %(message)s")
    logging.getLogger("tensorflow").setLevel(logging.WARNING)

    parser = argparse.ArgumentParser(description='Aspect Command Line Tool for English.')
    parser.add_argument('config', type=str, help='Classifier''s Configuration file.')

    subparsers = parser.add_subparsers(title='subcommands', description='Available commands')

    parser_gs = subparsers.add_parser('file', help='Reads from the Gold Standard file.')
    parser_gs.add_argument('gsfile', type=str, help='Gold Standard file.')
    parser_gs.add_argument('--threshold', type=int, default=0, help='Cut below this threshold. Default is %(default)s')
    parser_gs.add_argument('--limit', type=int, default=0, help='Max number of entries to read from the GS file.')
    parser_gs.add_argument('--out', type=str, help='Output file in CSV format.')
    parser_gs.set_defaults(func=__gsfilecmd)

    parser_txt = subparsers.add_parser('txt', help='Compute Aspects of a text excerpt.')
    parser_txt.add_argument('message', type=str, help='Text message to be evaluated.')
    parser_txt.set_defaults(func=__txtcmd)

    args = parser.parse_args()

    if 'func' not in args:
        parser.print_help()
        sys.exit(-1)

    cfg = load_from_file(args.config)
    word_model = create_word_embedding_model(English, cfg)
    extractor = create_aspect_analyzer(English, cfg, word_model)
    args.func(args, extractor)



