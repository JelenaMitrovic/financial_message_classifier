# -*- coding: utf8 -*-
# !/usr/bin/python


import json
import time
import unittest

from service import app
from ssix.classifier import SSIXClassifierWithTranslation
from ssix.config import load_defaults

_cfg = load_defaults()

# Overriding defaults to make sense on development environment
_cfg.ssix.models.word_embeddings.english.path = \
    "./moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin"

_cfg.ssix.aspects.external_tools.root_path = "../../tools"


class SSIXFinancialClassifierIntegrationTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        app.classifier = SSIXClassifierWithTranslation(_cfg)

    def testGet(self):
        with app.test_client() as client:
            response = client.get("/")
            self.assertEqual(200, response.status_code)
            self.assertEqual("application/json", response.content_type)
            data = json.loads(response.data.decode("utf-8"))
            self.assertTrue("host" in data)
            self.assertEqual("localhost", data["host"])
            self.assertTrue("classifier" in data)
            self.assertTrue("timestamp" in data)
            self.assertTrue(int(time.time()) >= data["timestamp"])

    def testPostEmpty(self):
        with app.test_client() as client:
            response = client.post("/")
            self.assertEqual(400, response.status_code)
            self.assertEqual("application/json", response.content_type)
            data = json.loads(response.data.decode("utf-8"))
            self.assertTrue("error" in data)
            self.assertEqual("Processing empty data", data["error"])
            self.assertFalse("data" in data)
            self.assertTrue("timestamp" in data)
            self.assertTrue(int(time.time()) >= data["timestamp"])

    def testPost(self):
        with app.test_client() as client:
            response = client.post("/", data="$GOOGL is doing really great", content_type="text/plain")
            self.assertEqual(200, response.status_code)
            self.assertEqual("application/json", response.content_type)
            data = json.loads(response.data.decode("utf-8"))
            self.assertTrue("sentiment" in data)
            self.assertTrue(data["sentiment"] > 0)
            self.assertTrue("timestamp" in data)
            self.assertTrue(int(time.time()) >= data["timestamp"])


if __name__ == "__main__":
    unittest.main()
