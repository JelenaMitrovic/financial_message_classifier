# -*- coding: utf-8 -*-

import unittest

from ssix.ner import NER


class NERTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.ner = NER()

    def test1(self):
        self.go("@CGrantWSJ You highlight the REAL risk to the $VRX/spec pharma businesses. Payers who won't pay anymore! $ENDP $HZNP $MNK")

    def test2(self):
        self.go("BP wins right to appeal some Gulf spill damages claims")

    def test3(self):
        self.go("Novo Nordisk and AstraZeneca seek tonic from key drug trials")

    def test4(self):
        self.go("FDA panel backs Glaxo asthma drug for adults, not adolescents")

    def test5(self):
        self.go("Apple's iPhone SE: The Sold Out Flop $AAPL https://t.co/z9vrtpMR0W")

    def test6(self):
        self.go("Berkshire applies to boost Wells Fargo stake above 10 percent")

    def test7(self):
        self.go("AB InBev to sell more SAB assets as seeks EU deal approval")

    def test8(self):
        self.go("$TSLA Call sweepers stepping up this morning.  $230 will have to wait.")

    def test9(self):
        self.go("Is #Facebook's user engagement falling? https://t.co/tqMMt7N16X $FB https://t.co/sedqRWgdkt")

    def go(self, text):
        r = self.ner.resolve(text)
        print(text, r)
        self.assertTrue(r)