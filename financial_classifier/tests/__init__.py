# -*- coding: utf8 -*-

import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(levelname)s %(name)s %(message)s")
logging.getLogger("tensorflow").setLevel(logging.WARNING)
logger = logging.getLogger()
logger.info("Logging configuration for testing.")
