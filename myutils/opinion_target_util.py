import myutils.twokenize as twokenize
from nltk.tag.stanford import StanfordPOSTagger
import os
import re
this_dir = os.path.dirname(__file__)



STANFORD_MODEL_FOLDER = os.path.join(this_dir,
                                         "../stanford-postagger-full-2016-10-31/models")
STANFORD_MODEL_TAGGER = os.path.join(this_dir,
                                         "../stanford-postagger-full-2016-10-31/models/english-bidirectional-distsim.tagger")
STANFORD_MODEL_FILE = os.path.join(this_dir,
                                       "../stanford-postagger-full-2016-10-31/models/gate-EN-twitter.model")
STANFORD_JAR = os.path.join(this_dir,
                                "../stanford-postagger-full-2016-10-31/stanford-postagger.jar")


def load_postag_model():
    st = StanfordPOSTagger(STANFORD_MODEL_FILE, path_to_jar=os.path.join(this_dir, STANFORD_JAR),
                           encoding='utf8')
    return st

def get_tweets_postag(sentence,st):

    tokenizer = st.tag(twokenize.tokenizeRawTweetText(sentence.strip()))
    return tokenizer

def get_sentence_postag(sentence, pos_model):
    normalized_sentence = normalize_sentence(sentence)
    sentence_wo_double_space = re.sub("(\s)+"," ",sentence)
    pos_sentence = get_tweets_postag(sentence_wo_double_space,pos_model)
    splited_sentence = sentence_wo_double_space.split()
    sentence_IOB =[]
    for i, sentence_token in enumerate(normalized_sentence.split()):
        sentence_IOB.append((splited_sentence[i], pos_sentence[i][1]))
    return sentence_IOB

def normalize_sentence(sentence):
    new_sentence = re.sub("(\s)+"," ",sentence)
    new_sentence = new_sentence.lower()
    return new_sentence
