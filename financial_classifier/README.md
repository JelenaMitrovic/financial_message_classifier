
SSIX - Financial Classifier
===========================

   * [Features:](#features)
   * [Build &amp; Installation](#build--installation)
      * [Container (recommended)](#container-recommended)
      * [Local (development)](#local-development)
      * [Required Data](#required-data)
   * [Running the service](#running-the-service)
      * [Local](#local)
      * [Docker Container](#docker-container)
   * [Querying the service](#querying-the-service)
   * [Aspect Extraction](#aspect-extraction)
      * [References](#references)
      * [Preparation](#preparation)
      * [Running](#running)
         * [Single text input](#single-text-input)
         * [File input](#file-input)
      * [The Algorithm](#the-algorithm)



# Features
 
* Machine Learning and Rule Based sentiment analysis engine
* Multilingual support through Machine Translation
* Named Entity Recognition for the financial domain
* Sentiment for each recognized relevant Named Entity
* Aspect/Feature extraction and its sentiment
  
# Build & Installation

These are the possible setups depending on your needs.

## Container (recommended)

It requires only [Docker](https://docs.docker.com/engine/installation), then you'll just need to run:
 
```
$ docker build -t ssix:financial_classifier .
```
  
.. and wait for a while.

In case you want a more compact image, you can skip to embed the models by sending a flag to the build:

```
$ docker build --build-arg EMBEDDED_MODELS=false -t ssix:financial_classifier .
```

Then you would need to mount the storage volume with the models when running the container (see below)

## Local (development)
 
Make sure your environment satisfies:

* Linux/Unix environment
* Python __3x__, recommended 3.4 or later
* Java 8, OpenJDK is recommended
* [Moven](https://bitbucket.org/ssix-project/moven) to install de machine/deep learning models
* [VirtualEnv](http://docs.python-guide.org/en/latest/dev/virtualenvs/) is highly recommended

Follow the steps:

1. Install OS dependencies: 

Example For Debian Systems.
    
```
$ sudo apt-get install python-dev libhdf5-dev python-h5py libenchant-dev libssl-dev
```
 
2. Install the project's dependencies:
 
First make sure you have the latest `pip`.

```
$ pip install -U pip
```

Then install the project's requirements.

```
$ pip install -r requirements.txt
```
 
3. Unit testing (optional)

Run the unit tests. Caveat: this can fail because depends on data models and internet connection.

```
$ nosetests
```

## Required Data

Before running you have to fetch some data regardless of the installation procedure above.

1. Retrieve the required distributional models with the [moven](https://bitbucket.org/ssix-project/moven) tool:

```
$ moven models.txt
```

If you get this error: _moven error retrieving 'eu.ssix.models:google-news-vectors-negative300:0.1'_ 
You'll need to register the [custom repository](https://maven.apache.org/settings.html#Servers).
    
```
$ cat <<EOF >> ~/.jip_config
   
    [repos:ssix]
    uri=https://nexus.ssix-project.eu/repository/maven-releases/
    type=remote
    EOF
```

2. Download required NLTK corpora. __Skip if you're using docker environment.__

```
$ python -m nltk.downloader -d ~/nltk_data vader_lexicon stopwords sentiwordnet punkt wordnet
```

# Running the service

## Local

Make a copy of _ssix/resources/config.yaml_ to customize the paths to the models and external resources.

```
$ python service.py <path-to-your/config.yaml> 
```
 
## Container

If you embedded the models on the image, you just need:

```
$ docker run -p 5000:5000 -v <TOOLS-PATH>:/opt/ssix/financial-classifier/tools ssix:financial_classifier
```

Using docker you'll have to mount the data volumes required by the container. You will need the moven directory holding 
the models and currently the project depend on some Stanford and CMU Java tools that can be found in the directory 
_wp4-pipeline/tools_ of this project.
 
All paths must be absolute.

```
$ docker run -p 5000:5000 -v <MOVEN-MODELS-PATH>:/opt/ssix/financial-classifier/moven -v <TOOLS-PATH>:/opt/ssix/financial-classifier/tools ssix:financial_classifier
```

__Note__: If you built the image with `EMBEDDED_MODELS=false` then you only need to mount the `<TOOLS-PATH>`.

# Querying the service 

You can use any HTTP client to query the service. The following examples are using [HTTPie](https://httpie.org/). If you try
for example _cURL_ be aware of your shell substitutions that would happen when the input text has entities like `$AAPL`.

__Note__: If you're in a python virtualenv you can install _HTTPie_ with `pip install httpie`     

From _Native English_ (default)

```
$ echo '@ByrneRWS $AAPL using $GOOGL for cloud' | http --body POST 'localhost:5000'
```
```json
{
    "aspects": {
        "@ByrneRWS $AAPL using $GOOGL for cloud": {
            "aspect_name": "technical_indicator", 
            "aspect_text": "using $GOOGL for cloud", 
            "score": 0.11365014314651489
        }
    }, 
    "data": "@ByrneRWS $AAPL using $GOOGL for cloud", 
    "sentiment": 0.26009833812713623, 
    "targets": {
        "aapl": {
            "label": "$aapl", 
            "sentiment": 0.26009833812713623
        }, 
        "goog": {
            "label": "$goog", 
            "sentiment": 0.26009833812713623
        }
    }, 
    "timestamp": 1495619354
}
```

To use the service with [Machine Translation](https://en.wikipedia.org/wiki/Machine_translation) you'll have to pass the URL parameter _lang_.
 
From _Portuguese_:
 
```
echo '$AAPL foi divertido brincar com vocês.' | http --body 'localhost:5000?lang=pt'
```
```json
{
    "aspects": {
        "$AAPL was fun to play with you.": {
            "aspect_name": "current_price", 
            "aspect_text": "was fun", 
            "score": 0.12068408727645874
        }
    }, 
    "data": "$AAPL foi divertido brincar com vocês.", 
    "sentiment": 0.6908, 
    "targets": {
        "aapl": {
            "label": "$aapl", 
            "sentiment": 0.6908
        }
    }, 
    "timestamp": 1495619714
}
```

From _Spanish_:
 
```
echo '$AAPL fue divertido jugar con ustedes.' | http --body 'localhost:5000?lang=es'
```
```json
{
    "aspects": {
        "$AAPL was fun to play with you.": {
            "aspect_name": "current_price", 
            "aspect_text": "was fun", 
            "score": 0.12068408727645874
        }
    }, 
    "data": "$AAPL fue divertido jugar con ustedes.", 
    "sentiment": 0.6908, 
    "targets": {
        "aapl": {
            "label": "$aapl", 
            "sentiment": 0.6908
        }
    }, 
    "timestamp": 1495619907
}
```

From _Romanian_:
 
```
echo '$AAPL a fost distractiv joc cu voi toți.' | http --body 'localhost:5000?lang=ro'
```
```json
{
    "aspects": {
        "$AAPL it was fun playing with all of you.": {}
    }, 
    "data": "$AAPL a fost distractiv joc cu voi toți.", 
    "sentiment": 0.6249, 
    "targets": {
        "aapl": {
            "label": "$aapl", 
            "sentiment": 0.6249
        }
    }, 
    "timestamp": 1495620021
}
```
 
From _German_:

```
echo '$AAPL Es hat Spaß gemacht, mit dir zu spielen.' | http --body 'localhost:5000?lang=de'
```
```json
{
    "aspects": {
        "$AAPL, it has been fun to play with friends.": {
            "aspect_name": "current_price", 
            "aspect_text": "fun", 
            "score": 0.11532372236251831
        }
    }, 
    "data": "$AAPL Es hat Spaß gemacht, mit dir zu spielen.", 
    "sentiment": 0.8316, 
    "targets": {
        "aapl": {
            "label": "$aapl", 
            "sentiment": 0.8316
        }
    }, 
    "timestamp": 1495620102
}
```

_As the source language is first translated to English all the outputs are the same._

# Aspect Extraction

## References

1. [Bing Liu "Sentiment Analysis and Opinion Mining"](https://www.cs.uic.edu/~liub/FBS/SentimentAnalysis-and-OpinionMining.pdf), chapter 5.3 (pp. 67 ff.)

## Preparation

Follow the instruction outlined above to setup the service locally.

## Running

There's a built-in help for the command-line tool.

```commandline
python -m ssix.aspect.extractor -h
usage: extractor.py [-h] config {file,txt} ...

Aspect Command Line Tool for English.

positional arguments:
  config      Classifiers Configuration file.

optional arguments:
  -h, --help  show this help message and exit

subcommands:
  Available commands

  {file,txt}
    file      Reads from the Gold Standard file.
    txt       Compute Aspects of a text excerpt.
```

### Single text input

A single text input example. Be aware of the quotes.
See this specific command help with `python -m ssix.aspect.extractor txt -h`

```commandline
python -m ssix.aspect.extractor config-dev.yaml txt '2 Turnaround Buys For 2016? BHP Billiton plc And Home Retail Group Plc'
```
```commandline
INFO ssix.models Loading /home/lsouza/ws/smx/wp4-pipeline/components/financial_classifier/moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin model..
INFO gensim.models.word2vec loading projection weights from /home/lsouza/ws/smx/wp4-pipeline/components/financial_classifier/moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin
INFO gensim.models.word2vec loaded (3000000, 300) matrix from /home/lsouza/ws/smx/wp4-pipeline/components/financial_classifier/moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin
INFO ssix.models Model loaded sucessfuly.
INFO ssix.aspect.extractor Initialising analyser 'twitie'...
INFO ssix.aspect.extractor Loading lexicalisations...
INFO ssix.aspect.extractor Extracting aspects of '2 Turnaround Buys For 2016? BHP Billiton plc And Home Retail Group Plc'..
{'score': 0.14698092639446259, 'aspect_name': 'accounting', 'aspect_text': 'Retail Group Plc'}
```

### File input

Two JSON files based on the microblog and headline Gold Standards can be found in `ssix/aspect/data/gs_for_testing`. Currently the script can only accept this kind of format.
For this example the computation was limited to 5 entries. See this specific command help with `python -m ssix.aspect.extractor file -h`.

```commandline
python -m ssix.aspect.extractor config-dev.yaml file ssix/aspect/data/gs_for_testing/Headline_Trainingdata.json --limit 5 --out headline_analysed.csv
```

After a while ..

```commandline
INFO ssix.models Loading /home/lsouza/ws/smx/wp4-pipeline/components/financial_classifier/moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin model..
INFO gensim.models.word2vec loading projection weights from /home/lsouza/ws/smx/wp4-pipeline/components/financial_classifier/moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin
INFO gensim.models.word2vec loaded (3000000, 300) matrix from /home/lsouza/ws/smx/wp4-pipeline/components/financial_classifier/moven/google-news-vectors-negative300/GoogleNews-vectors-negative300.bin
INFO ssix.models Model loaded sucessfuly.
INFO ssix.aspect.extractor Initialising analyser 'twitie'...
INFO ssix.aspect.extractor Loading lexicalisations...
Analyzing 5 messages
INFO ssix.aspect.extractor Extracting aspects of '2 Turnaround Buys For 2016? BHP Billiton plc And Home Retail Group Plc'..
INFO ssix.aspect.extractor Extracting aspects of '3G Capital, Warren Buffett's Favorite Partner in Deals Worth Billions'..
INFO ssix.aspect.extractor Extracting aspects of '5 Years After BP Spill: What's Changed in Offshore Drilling'..
INFO ssix.aspect.extractor Extracting aspects of 'AB InBev Holds Talks With Regulators About California Deals'..
INFO ssix.aspect.extractor Extracting aspects of 'AB InBev approaches SABMiller to explore $250bn tie-up'..
Writing 5 entries in headline_analysed.csv
```

The output is a CSV file.

```commandline
cat headline_analysed.csv 
```
```commandline
index,message,aspect_name,aspect_text,score
0,2 Turnaround Buys For 2016? BHP Billiton plc And Home Retail Group Plc,accounting,Retail Group Plc,0.14698092639446259
1,"3G Capital, Warren Buffett's Favorite Partner in Deals Worth Billions",accounting,3G Capital,0.15872207283973694
2,5 Years After BP Spill: What's Changed in Offshore Drilling,ratings,Changed in Offshore Drilling,0.08525891602039337
3,AB InBev Holds Talks With Regulators About California Deals,accounting,Holds Talks With Regulators About California Deals,0.08117558062076569
4,AB InBev approaches SABMiller to explore $250bn tie-up,earnings,approaches SABMiller,0.09850757569074631
```


## The Algorithm

Being a single text input or the file the Aspect Extractor will do the following:

1. Input text is POS-tagged and chunked (by default the "Twitie" tagger is used, but the alternative "CMU" tagger can be swapped in by changing the `init` arguments to `AspectExpressionExtractor`)
2. Selected words from the identified chunks are compared to lexicalisations from the aspects model. This comparison is based on cosine from the w2v model.
3. For each message, the most likely aspect expressions are returned, along with the kind of aspect (*Note*: Currently, the Extractor outputs _all_ the chunks, ranked for each aspect (testing/debugging mode)).
