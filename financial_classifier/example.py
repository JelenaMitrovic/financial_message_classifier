# -*- coding: utf-8 -*-

import logging
import sys

from ssix.classifier import SSIXClassifier
from ssix.config import load_defaults
from ssix.analyzers import English

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(levelname)s %(name)s %(message)s")

test_sentences = """$FB is profitable but $IBM sucks.
Despite its profit, $FB sucks.
$FB sucks despite its profit.
$AAPL is very cool.
$IBM is not so cool.
$FB, $IBM and $GOOGL well done!
$IBM is a disaster.
$IBM is lame.
$IBM is extremely lame!
apple is extremely lame!
Boeing in awesome!
$FB shows a slight uptrend in the last 2 days.
$FB shows a incredibly strong uptrend in the last 2 days.
SUCKS!"""

classifier = SSIXClassifier(English, load_defaults())

for sentence in test_sentences.split('\n'):
    sentiment, entities, aspects = classifier.classify(sentence)
    print(sentence, sentiment, entities, aspects)
