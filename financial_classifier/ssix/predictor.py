# -*- coding: utf-8 -*-

import logging
import math
import re
import unicodedata
from collections import Counter

import nltk
import numpy as np
from nltk.corpus import sentiwordnet as swn
from nltk.corpus import wordnet as wn

logger = logging.getLogger(__name__)


def _strip_accents(text):
    text = unicodedata.normalize('NFD', text)
    text = text.encode('ascii', 'ignore')
    text = text.decode("utf-8")
    return str(text)


def _cosine_similarity(sent1, sent2):
    word = re.compile(r'\w+')

    def get_cosine(vec1, vec2):
        intersection = set(vec1.keys()) & set(vec2.keys())
        numerator = sum([vec1[x] * vec2[x] for x in intersection])

        sum1 = sum([vec1[x] ** 2 for x in vec1.keys()])
        sum2 = sum([vec2[x] ** 2 for x in vec2.keys()])
        denominator = math.sqrt(sum1) * math.sqrt(sum2)

        if not denominator:
            return 0.0
        else:
            return float(numerator) / denominator

    def text_to_vector(text):
        words = word.findall(text)
        return Counter(words)

    vector1 = text_to_vector(sent1)
    vector2 = text_to_vector(sent2)
    cosine = get_cosine(vector1, vector2)
    return cosine


class MicroblogPredictor(object):
    # TODO: Move this parameter to WordEmbeddingModel or figure out on how to get rif of it
    __word_embedding_size = 300
    __max_sentence_size = 50

    def __init__(self, word_embedding_model, predictor_model):
        self.__model = predictor_model
        self.__word_embedding_model = word_embedding_model

    def predict(self, sentence):
        sentence = re.sub(r'(\$[a-z]+)', lambda pat: pat.group(1).upper(), sentence.strip(), re.IGNORECASE)
        in_tuple = (1, self.__max_sentence_size, self.__word_embedding_size)
        sentences = np.empty(in_tuple, dtype='float')
        sentences.fill(-1.0)

        for j, token in enumerate(nltk.word_tokenize(sentence)):
            if j >= self.__max_sentence_size:
                break
            if token in self.__word_embedding_model:
                sentences[0][j] = self.__word_embedding_model[token]
            else:
                sentences[0][j] = [-1] * self.__word_embedding_size

        # generate sentiwordnet score vectors
        senti_scores = [
            self.get_sentwordnet_sentence_scores_local(nltk.word_tokenize(sentence), self.__max_sentence_size)
        ]

        senti_scores_array = np.array(senti_scores)

        prediction = self.__model.predict([sentences, senti_scores_array], batch_size=1)
        return float(prediction[0][0])

    @staticmethod
    def get_sentwordnet_sentence_scores_local(sentence, max_sentence):
        """
        get sentiwordnet scores vector
        """
        in_tuple = (max_sentence, 2)
        pos_neg_score = np.empty(in_tuple, dtype="float")
        pos_neg_score.fill(-1.0)
        sent = ' '.join(sentence)

        for i, token in enumerate(sentence):
            max_cosine_score = -1.0
            token_senses = list(swn.senti_synsets(_strip_accents(token.lower())))
            if i < max_sentence:
                for senti_sense in token_senses:
                    sense = re.findall("'([^:]*)'", senti_sense.unicode_repr())[0]
                    cosine_score_value = _cosine_similarity(wn.synset(sense).definition(), sent)
                    if cosine_score_value >= max_cosine_score:
                        pos_neg_score[i][0] = 1000 * senti_sense.pos_score()
                        pos_neg_score[i][1] = 1000 * senti_sense.neg_score()
                        max_cosine_score = cosine_score_value
        return pos_neg_score


