# -*- coding: utf8 -*-

from os.path import dirname, join, abspath, expanduser

import logging
import gensim

_logger = logging.getLogger(__name__)


class DummyModel(object):

    def __init__(self):
        _logger.warning("Using Dummy Word Embedding Model!")

    def __contains__(self, item): return False

    def __getitem__(self, item): pass


class W2VModel(object):
    __logger = logging.getLogger(__name__)

    def __init__(self, w2v_path):
        if not w2v_path:
            raise ValueError("Invalid model path")

        self.__logger.info("Loading %s model..", w2v_path)
        self.w2v = gensim.models.Word2Vec.load_word2vec_format(fname=w2v_path, binary=True)
        self.__logger.info("Model loaded sucessfuly.")

    def __getitem__(self, item):
        return self.w2v[item]

    def __contains__(self, item):
        return item in self.w2v


def create_word_embedding_model(language, config):
    model_type = config.ssix.models.word_embeddings[language.name].type

    if model_type == 'dummy':
        return DummyModel()
    elif model_type == 'w2v':
        return W2VModel(abspath(expanduser(config.ssix.models.word_embeddings[language.name].path)))

    raise ValueError("Unknown model type: {}".format(model_type))


def create_predictor_model(language):
    model_arch_file = join(dirname(__file__), "resources", "keras", "regression_arch.yaml")
    model_weights_file = join(dirname(__file__), "resources", "keras", "{}.hdf5".format(language.name))
    _logger.info("Creating predictor for %s [%s, %s]", language, model_arch_file, model_weights_file)
    from keras.models import model_from_yaml
    with open(model_arch_file, 'r') as f:
        model = model_from_yaml(f.read())
        model.load_weights(model_weights_file)
        return model


def create_stopwords_set(language):
    from nltk.corpus import stopwords
    return stopwords.words(language.name)
