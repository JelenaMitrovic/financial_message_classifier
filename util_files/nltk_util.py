__author__ = 'macedo'
import nltk
import unicodedata
import numpy as np
from nltk.stem.wordnet import WordNetLemmatizer
from util_files.string_util import normalize_sentence
from nltk.corpus import wordnet as wn
from nltk.corpus import sentiwordnet as swn
from util_files.cosine import cosine_similarity
import re
#get the sentence tokens
def get_sent_tokenized(sentence):
    return nltk.word_tokenize(sentence)

#get the tokens of sentence with its correspondent postag
def get_sent_pos_tags(sentence):
    tokenized_sentence = []
    if not(isinstance(sentence,list)):
        tokenized_sentence = get_sent_tokenized(sentence)
    else:
        tokenized_sentence = sentence
    return nltk.pos_tag(tokenized_sentence)

#get the list of sentence postags
def get_pos_tags(sentence):
    postags = []
    postags_sentence = get_sent_pos_tags(sentence)
    for postagged_token in postags_sentence:
        postags.append(postagged_token[1])
    return postags



#get the lemmatized tokens
def get_lemmatized_tokens(sentence):
    lemmas = []
    tokenized_sentence =[]
    if not(isinstance(sentence,list)):
        tokenized_sentence = get_sent_tokenized(sentence)
    else:
        tokenized_sentence = sentence    
    postag_list = get_pos_tags(sentence)
    wordnet_lemmatizer = WordNetLemmatizer()
    for i,token in enumerate(tokenized_sentence):
        tag = penn_to_wn(postag_list[i])
        if tag == None:           
            lemmas.append(token)
        else:
            lemmas.append(wordnet_lemmatizer.lemmatize(token,pos=tag))
    return lemmas

#get the lemmatized tokens
def get_lemmatized_token(token,tag):
    wn_tag = penn_to_wn(tag)
    wordnet_lemmatizer = WordNetLemmatizer()
    if wn_tag == None :
        return token
    else:
        return wordnet_lemmatizer.lemmatize(token,pos=wn_tag)

def is_noun(tag):
    return tag in ['NN', 'NNS', 'NNP', 'NNPS']


def is_verb(tag):
    return tag in ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']


def is_adverb(tag):
    return tag in ['RB', 'RBR', 'RBS']


def is_adjective(tag):
    return tag in ['JJ', 'JJR', 'JJS']


def penn_to_wn(tag):
    if is_adjective(tag):
        return wn.ADJ
    elif is_noun(tag):
        return wn.NOUN
    elif is_adverb(tag):
        return wn.ADV
    elif is_verb(tag):
        return wn.VERB
    else:
        return None




def vocabulary_to_id(vocabulary):
    pos_dict = dict()
    #in_tuple = (len(vocabulary),1)
    #pos_to_id = np.empty(in_tuple, dtype= "int")
    #pos_to_id.fill(-1)
    cont=0
    for i,pos in enumerate(vocabulary):
        if not(penn_to_wn(pos) in pos_dict.keys()):
            pos_dict[penn_to_wn(pos)]= cont
            cont+=1
    return pos_dict

def generate_pos_vocab(sentences):
    pos_vocab = []
    cont=0
    for sentence in sentences:
        splited_line = normalize_sentence(sentence).split("@")
        tokenized_sentence = get_lemmatized_tokens(splited_line[0])
        pos_sentence =  get_pos_tags(splited_line[0])
        for pos in pos_sentence:
            if pos is not pos_vocab:
                pos_vocab.append(pos)

    return pos_vocab


def get_sentwordnet_sentence_scores_local(sentence,max_sentence):
    
    in_tuple = (max_sentence,2)
    pos_neg_score = np.empty(in_tuple, dtype= "float")
    pos_neg_score.fill(-1.0)
    sent = ' '.join(sentence)

    for i,token in enumerate(sentence):
        #print(str(i))
        max_cosine_score = -1.0
        token_senses = list(swn.senti_synsets(token))
        if i < max_sentence :
            for senti_sense in token_senses:
                sense = re.findall("'([^:]*)'",senti_sense.unicode_repr())[0]
                cosine_score_value = cosine_similarity(wn.synset(sense).definition(),sent)
                if cosine_score_value >= max_cosine_score :
                    pos_neg_score[i][0] = 1000*senti_sense.pos_score()
                    pos_neg_score[i][1] = 1000*senti_sense.neg_score()
                    max_cosine_score = cosine_score_value
    return pos_neg_score
