#!/usr/bin/env python
# -*- coding: utf-8 -*-

# TODO: This is not production ready.

import logging
import re
import sys
import time
from threading import Thread

from flask import Flask, request, jsonify, make_response

from size import get_size, sizeof_fmt
from ssix.classifier import SSIXClassifierWithTranslation
from ssix.config import load_from_file

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format="%(levelname)s %(name)s %(message)s")

app = Flask(__name__)


@app.route("/", methods=["POST"])
def serve():
    try:
        lang = request.args.get("lang", "en")
        data = request.get_data().decode('utf-8').strip()

        if len(data) > 1 and data[0] == "\"" and data[-1] == "\"":
            data = data[1:-1]

        if len(data) == 0:
            app.logger.warning("Processing empty data")
            response = {
                'error': "Processing empty data",
                'timestamp': int(time.time())
            }
            return make_response(jsonify(response), 400)

        # PIPELINE-257 (1)
        clean_data = re.sub("(^|\s|\$)?GOOG($|\s)", " $GOOGL ", data)
        clean_data = re.sub("(^|\s|\$)?goog($|\s)", " $googl ", clean_data)
        clean_data = " ".join(clean_data.split())

        sentiment, targets, aspects = app.classifier.classify(clean_data, language_code=lang)

        # PIPELINE-257 (2)
        if "googl" in targets:
            targets["goog"] = targets.pop("googl")
            targets["goog"]["label"] = targets["goog"]["label"][:-1]

        app.logger.debug("sentiment for '%s' is %f" % (clean_data, sentiment))
        response = {
            'data': data,
            'sentiment': sentiment,
            'targets': targets,
            'aspects': aspects,
            'timestamp': int(time.time())
        }
        return jsonify(response)
    except Exception as e:
        try:
            data
        except UnboundLocalError:
            data = None
        except NameError:
            data = None
        app.logger.error("Error processing '%s': %s", data, str(e))
        response = {
            'data': data,
            'error': str(e),
            'timestamp': int(time.time())
        }
        return make_response(jsonify(response), 500)


@app.route("/", methods=["HEAD", "GET"])
def info():
    response = {}
    host = request.host.split(":")
    response['host'] = host[0]
    if len(host) > 1:
        response['port'] = int(host[1])
    response['classifier'] = str(app.classifier)
    response['timestamp'] = int(time.time())
    return jsonify(response)


if __name__ == "__main__":
    config = load_from_file(sys.argv[1])
    app.classifier = SSIXClassifierWithTranslation(config)
    Thread(target=lambda thisapp: logging.debug("Service app currently uses %s of memory",
                                                sizeof_fmt(get_size(thisapp))), args=([app])).start()
    port = int(sys.argv[2]) if len(sys.argv) > 2 else 5000
    app.run(host="0.0.0.0", port=port, threaded=True)
