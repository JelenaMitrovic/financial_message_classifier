import re, math
from collections import Counter

def cosine_similarity(sent1, sent2): # 
    """ 
    Calculates cosine between 2 sentences/documents.
    """
    WORD = re.compile(r'\w+')
    def get_cosine(vec1, vec2):
        intersection = set(vec1.keys()) & set(vec2.keys())
        numerator = sum([vec1[x] * vec2[x] for x in intersection])
        
        sum1 = sum([vec1[x]**2 for x in vec1.keys()])
        sum2 = sum([vec2[x]**2 for x in vec2.keys()])
        denominator = math.sqrt(sum1) * math.sqrt(sum2)
        
        if not denominator:
            return 0.0
        else:
            return float(numerator) / denominator

    def text_to_vector(text):
        words = WORD.findall(text)
        return Counter(words)
    
    vector1 = text_to_vector(sent1)
    vector2 = text_to_vector(sent2)
    cosine = get_cosine(vector1, vector2)
    return cosine

def abs_cosine_similarity(word_vector1, word_vector2):
    """ 
    Calculates cosine between 2 sentences/documents.
    """
    def get_cosine(vec1, vec2):

        if len(vec1) != len(vec2):
            return 0.0
        numerator =0.0
        sum1 = 0.0
        sum2 = 0.0
        for x in range(len(vec1)):

            numerator += vec1[x] * vec2[x]

            sum1 += math.pow(vec1[x],2)
            sum2 += math.pow(vec2[x],2)

        denominator = math.sqrt(sum1) * math.sqrt(sum2)
        if not denominator:
            return 0.0
        else:
            return numerator / denominator
    cosine = get_cosine(word_vector1, word_vector2)
    return (-1)*cosine if cosine < 0 else cosine

